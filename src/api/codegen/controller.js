import { success, notFound, authorOrAdmin } from '../../services/response/'
import { Codegen } from '.'

export const create = ({ user, bodymen: { body } }, res, next) =>
  Codegen.create({ ...body, user })
    .then((codegen) => codegen.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Codegen.find(query, select, cursor)
    .populate('user')
    .then((codegens) => codegens.map((codegen) => codegen.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Codegen.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((codegen) => codegen ? codegen.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  Codegen.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((codegen) => codegen ? Object.assign(codegen, body).save() : null)
    .then((codegen) => codegen ? codegen.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ user, params }, res, next) =>
  Codegen.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((codegen) => codegen ? codegen.remove() : null)
    .then(success(res, 204))
    .catch(next)

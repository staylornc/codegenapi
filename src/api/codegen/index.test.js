import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Codegen } from '.'

const app = () => express(apiRoot, routes)

let userSession, anotherSession, codegen

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const anotherUser = await User.create({ email: 'b@b.com', password: '123456' })
  userSession = signSync(user.id)
  anotherSession = signSync(anotherUser.id)
  codegen = await Codegen.create({ user })
})

test('POST /codegens 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, blocksrc: 'test', generatedcode: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.blocksrc).toEqual('test')
  expect(body.generatedcode).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('POST /codegens 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /codegens 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /codegens/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${codegen.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(codegen.id)
})

test('GET /codegens/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /codegens/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${codegen.id}`)
    .send({ access_token: userSession, blocksrc: 'test', generatedcode: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(codegen.id)
  expect(body.blocksrc).toEqual('test')
  expect(body.generatedcode).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('PUT /codegens/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${codegen.id}`)
    .send({ access_token: anotherSession, blocksrc: 'test', generatedcode: 'test' })
  expect(status).toBe(401)
})

test('PUT /codegens/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${codegen.id}`)
  expect(status).toBe(401)
})

test('PUT /codegens/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: anotherSession, blocksrc: 'test', generatedcode: 'test' })
  expect(status).toBe(404)
})

test('DELETE /codegens/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${codegen.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /codegens/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${codegen.id}`)
    .send({ access_token: anotherSession })
  expect(status).toBe(401)
})

test('DELETE /codegens/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${codegen.id}`)
  expect(status).toBe(401)
})

test('DELETE /codegens/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: anotherSession })
  expect(status).toBe(404)
})

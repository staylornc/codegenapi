import { Codegen } from '.'
import { User } from '../user'

let user, codegen

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  codegen = await Codegen.create({ user, blocksrc: 'test', generatedcode: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = codegen.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(codegen.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.blocksrc).toBe(codegen.blocksrc)
    expect(view.generatedcode).toBe(codegen.generatedcode)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = codegen.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(codegen.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.blocksrc).toBe(codegen.blocksrc)
    expect(view.generatedcode).toBe(codegen.generatedcode)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})

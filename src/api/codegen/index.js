import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
import { JavaScript } from 'node-blockly/js';

const router = new Router()
const { blocksrc, generatedcode } = schema.tree
var Blockly = require('node-blockly');

function generateCodeFromBlocksAsync(res, blocksrc, language) {
  return new Promise((resolve, reject) => {
    if (!blocksrc) {
      throw new Error("Missing block source");
    }
    if (!language) {
      throw new Error("Missing language type");
    }

    try {
      var xml = Blockly.Xml.textToDom(blocksrc);
    }
    catch (e) {
      console.log(e);
      return
    }
   
    var workspace = new Blockly.Workspace();
    Blockly.Xml.domToWorkspace(xml, workspace);
    var code;
    switch (language) {
      case 'Python':
        code = Blockly.Python.workspaceToCode(workspace);
        break;
      case 'JavaScript':
        code = Blockly.JavaScript.workspaceToCode(workspace);
        break;
      case 'Lua':
        code = Blockly.Lua.workspaceToCode(workspace);
        break;
      case 'PHP':
        code = Blockly.PHP.workspaceToCode(workspace);
        break;
      case 'Dart':
        code = Blockly.Dart.workspaceToCode(workspace);
        break;
      default:
        code = Blockly.Lua.workspaceToCode(workspace);
        break;
    }

    // res.send("language: " + language + '</br>' 
    //     + "code: " +  '</br>' 
    //     + code);
    res.send(code);
    resolve();
  });
}

router.post('/generateCodeFromBlocks', (req, res, next) => { 
  generateCodeFromBlocksAsync(res, req.body, "Lua") 
// hardcoding this for now as IntelliMediaCore Http requests do not yet support post requests with a header
//  generateCodeFromBlocksAsync(res, req.body, req.header('targetLanguage')) 
  .catch(err => next(err));
});


router.get('/', function(req, res, next) {
  // let { directory, filename } = _getFilename(
  //   req.header('AppName'), 
  //   req.header('GroupName'), 
  //   req.param('Filename'));
  res.sendStatus(200)
  .catch(err => next(err));
});

/**
 * @api {post} /codegens Create codegen
 * @apiName CreateCodegen
 * @apiGroup Codegen
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam blocksrc Codegen's blocksrc.
 * @apiParam generatedcode Codegen's generatedcode.
 * @apiSuccess {Object} codegen Codegen's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Codegen not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ blocksrc, generatedcode }),
  create)

/**
 * @api {get} /codegens Retrieve codegens
 * @apiName RetrieveCodegens
 * @apiGroup Codegen
 * @apiUse listParams
 * @apiSuccess {Object[]} codegens List of codegens.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /codegens/:id Retrieve codegen
 * @apiName RetrieveCodegen
 * @apiGroup Codegen
 * @apiSuccess {Object} codegen Codegen's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Codegen not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /codegens/:id Update codegen
 * @apiName UpdateCodegen
 * @apiGroup Codegen
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam blocksrc Codegen's blocksrc.
 * @apiParam generatedcode Codegen's generatedcode.
 * @apiSuccess {Object} codegen Codegen's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Codegen not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ blocksrc, generatedcode }),
  update)

/**
 * @api {delete} /codegens/:id Delete codegen
 * @apiName DeleteCodegen
 * @apiGroup Codegen
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Codegen not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
// Blockly.Xml.blockToDom = function (block, opt_noId) {
//   var element = goog.dom.createDom('block');
//   // var element = goog.dom.createDom(block.isShadow() ? 'shadow' : 'block');
// 	element.setAttribute('type', block.type);
// 	if (!opt_noId) {
// 		element.setAttribute('id', block.id);
// 	}
// 	if (block.mutationToDom) {
// 		// Custom data for an advanced block.
// 		var mutation = block.mutationToDom();
// 		if (mutation && (mutation.hasChildNodes() || mutation.hasAttributes())) {
// 			element.appendChild(mutation);
// 		}
// 	}

// 	Blockly.Xml.allFieldsToDom_(block, element);

// 	var commentText = block.getCommentText();
// 	if (commentText) {
// 		var commentElement = goog.dom.createDom('comment', null, commentText);
// 		if (typeof block.comment == 'object') {
// 			commentElement.setAttribute('pinned', block.comment.isVisible());
// 			var hw = block.comment.getBubbleSize();
// 			commentElement.setAttribute('h', hw.height);
// 			commentElement.setAttribute('w', hw.width);
// 		}
// 		element.appendChild(commentElement);
// 	}

// 	if (block.data) {
// 		var dataElement = goog.dom.createDom('data', null, block.data);
// 		element.appendChild(dataElement);
// 	}

// 	for (var i = 0, input; input = block.inputList[i]; i++) {
// 		var container;
// 		var empty = true;
// 		if (input.type == Blockly.DUMMY_INPUT) {
// 			continue;
// 		} else {
// 			var childBlock = input.connection.targetBlock();
// 			if (input.type == Blockly.INPUT_VALUE) {
// 				container = goog.dom.createDom('value');
// 			} else if (input.type == Blockly.NEXT_STATEMENT) {
// 				container = goog.dom.createDom('statement');
// 			}
// 			var shadow = input.connection.getShadowDom();
// 			if (shadow && (!childBlock || !childBlock.isShadow())) {
// 				// NOTES: Manually added in for override. If you update this function at any point, ensure the equivalent is commented out
// 				// container.appendChild(Blockly.Xml.cloneShadow_(shadow));
// 			}
// 			if (childBlock) {
// 				container.appendChild(Blockly.Xml.blockToDom(childBlock, opt_noId));
// 				empty = false;
// 			}
// 		}
// 		container.setAttribute('name', input.name);
// 		if (!empty) {
// 			element.appendChild(container);
// 		}
// 	}
// 	if (block.inputsInlineDefault != block.inputsInline) {
// 		element.setAttribute('inline', block.inputsInline);
// 	}
// 	if (block.isCollapsed()) {
// 		element.setAttribute('collapsed', true);
// 	}
// 	if (block.disabled) {
// 		element.setAttribute('disabled', true);
// 	}
// 	if (!block.isDeletable() && !block.isShadow()) {
// 		element.setAttribute('deletable', false);
// 	}
// 	if (!block.isMovable() && !block.isShadow()) {
// 		element.setAttribute('movable', false);
// 	}
// 	if (!block.isEditable()) {
// 		element.setAttribute('editable', false);
// 	}

// 	var nextBlock = block.getNextBlock();
// 	if (nextBlock) {
// 		var container = goog.dom.createDom('next', null,
// 			Blockly.Xml.blockToDom(nextBlock, opt_noId));
// 		element.appendChild(container);
// 	}
// 	var shadow = block.nextConnection && block.nextConnection.getShadowDom();
// 	if (shadow && (!nextBlock || !nextBlock.isShadow())) {
// 		container.appendChild(Blockly.Xml.cloneShadow_(shadow));
// 	}

// 	return element;
// };

// Blockly.Xml.domToVariables = function(xmlVariables, workspace) {
//   // Had to change this to childNodes from children for headless generation
//   for (var i = 0, xmlChild; xmlChild = xmlVariables.childNodes[i]; i++) {
//     var type = xmlChild.getAttribute('type');
//     var id = xmlChild.getAttribute('id');
//     var name = xmlChild.textContent;

//     if (type == null) {
//       throw Error('Variable with id, ' + id + ' is without a type');
//     }
//     workspace.createVariable(name, type, id);
//   }
// };